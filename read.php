<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Randonnées</title>
    <link rel="stylesheet" href="css/basics.css" media="screen" title="no title" charset="utf-8">
  </head>
  <body>

<?php
DEFINE(SERVER, "localhost");
DEFINE(LOGIN, "root");
DEFINE(MDP, "root");
DEFINE(BASE, "reunion_island");

$connect = mysqli_connect(SERVER, LOGIN, MDP, BASE) or die("Erreur de connexion au serveur");
$result = mysqli_query($connect, "SELECT * FROM hiking");


?>

    <h1>Liste des randonnées</h1>
    <table>
      <tr>
        <th>Name</th>
        <th>Difficulty</th>
        <th>Distance [km]</th>
        <th>Duration</th>
        <th>Height Difference [m]</th>
        <th>Praticable ?</th>
      </tr>

  <?php
  while($data = mysqli_fetch_assoc($result)){
  ?>

    <tr>
      <td><?= $data["name"] ?></td>
      <td><?= $data["difficulty"] ?></td>
      <td><?= $data["distance"] ?></td>
      <td><?= $data["duration"] ?></td>
      <td><?= $data["height_difference"] ?></td>
      <td><?= $data["available"] ?></td>
      <td>
        <form action="/update.php" method="post">
          <input type="hidden" name="id" value="<?=$data["id"]?>">
          <button type="submit">Modifier</button>
        </form>
      </td>
      <td>
        <form action="/delete.php" method="post">
          <input type="hidden" name="id" value="<?=$data["id"]?>">
          <button type="submit">Supprimer</button>
        </form>
      </td>
    </tr>


  <?php } ?>

    </table>
  </body>
</html>
