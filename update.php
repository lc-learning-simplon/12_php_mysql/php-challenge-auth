<?php session_start(); 
  if ($_SESSION["id"] == "" || !isset($_SESSION) ) {
    header("Location: /login.php");
    die();
  }
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Ajouter une randonnée</title>
	<link rel="stylesheet" href="css/basics.css" media="screen" title="no title" charset="utf-8">
</head>
<body>
	<a href="/read.php">Liste des données</a>
	<h1>Ajouter</h1>

	<?php
	$connect = new PDO('mysql:host=localhost;dbname=reunion_island', 'root', 'root');
	$id = $_POST["id"];

	$sql = "SELECT * FROM hiking WHERE id = $id";
	foreach ($connect->query($sql) as $data) {
		$name = $data["name"];
		$difficutly = $data["difficulty"];
		$distance = $data["distance"];
		$duration = $data["duration"];
		$height_difference = $data["height_difference"];
		$available = $data["avalaible"];
	}

	?>


	<form action="/update.php" method="post">
		<input type="hidden" name="id" value="<?=$id?>">
		<div>
			<label for="name">Name</label>
			<input type="text" name="name" value="<?=$name?>">

		<div>
			<label for="difficulty">Difficulté</label>
			<select name="difficulty">
				<option value="<?=$difficutly?>" selected hidden><?=$difficutly?></option>
				<option value="très facile">Très facile</option>
				<option value="facile">Facile</option>
				<option value="moyen">Moyen</option>
				<option value="difficile">Difficile</option>
				<option value="très difficile">Très difficile</option>
			</select>
		</div>
		
		<div>
			<label for="distance">Distance</label>
			<input type="number" min=1 name="distance" value="<?=$distance?>">
		</div>
		<div>
			<label for="duration">Durée</label>
			<input type="duration" name="duration" value="<?=$duration?>">
		</div>
		<div>
			<label for="height_difference">Dénivelé</label>
			<input type="number" min=1 name="height_difference" value="<?=$height_difference?>">
		</div>
		<div>
			<label for="available">Praticable ?</label>
			<select name="available" id="available">
				<option value="<?=$available?>" selected hidden><?=$available?></option>
				<option value="Oui">Oui</option>
				<option value="Non">Non</option>
			</select>
		</div>
		<button type="submit" name="button">Envoyer</button>
	</form>

	<?php

	if (isset($_POST["name"])) {
		$id = $_POST["id"];
		$value1 = addslashes($_POST["name"]);
		$value2 = $_POST["difficulty"];
		$value3 = $_POST["distance"];
		$value4 = $_POST["duration"];
		$value5 = $_POST["height_difference"];
		$value6 = $_POST["available"];
		$sql = "UPDATE hiking SET name = '$value1', difficulty = '$value2', distance = '$value3', duration = '$value4', height_difference = '$value5', available = '$value6' WHERE id = '$id'";
		$connect->exec($sql);
		$err = $connect->errorInfo();
		if($err[2] != "") {
			print_r($err);
		}
		else {
			echo "Succès !";
		}

	}


	?>
</body>
</html>
