<?php session_start(); 
  if ($_SESSION["id"] == "" || !isset($_SESSION) ) {
    header("Location: /login.php");
    die();
  }
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Ajouter une randonnée</title>
	<link rel="stylesheet" href="css/basics.css" media="screen" title="no title" charset="utf-8">
</head>
<body>
	<a href="/read.php">Liste des données</a>
	<h1>Ajouter</h1>
	<form action="/create.php" method="post">
		<div>
			<label for="name">Name</label>
			<input type="text" name="name" value="">
		</div>

		<div>
			<label for="difficulty">Difficulté</label>
			<select name="difficulty">
				<option value="très facile">Très facile</option>
				<option value="facile">Facile</option>
				<option value="moyen">Moyen</option>
				<option value="difficile">Difficile</option>
				<option value="très difficile">Très difficile</option>
			</select>
		</div>
		
		<div>
			<label for="distance">Distance</label>
			<input type="number" min=1 name="distance" value="">
		</div>
		<div>
			<label for="duration">Durée</label>
			<input type="duration" name="duration" value="">
		</div>
		<div>
			<label for="height_difference">Dénivelé</label>
			<input type="number" min=1 name="height_difference" value="">
		</div>
		<div>
			<label for="available">Praticable ?</label>
			<select name="available" id="available">
				<option value="Oui">Oui</option>
				<option value="Non">Non</option>
			</select>
		</div>
		<button type="submit" name="button">Envoyer</button>
	</form>

	<?php

	$connect = new PDO('mysql:host=localhost;dbname=reunion_island', 'root', 'root');


	if (isset($_POST["name"])) {
		$value1 = $_POST["name"];
		$value2 = $_POST["difficulty"];
		$value3 = $_POST["distance"];
		$value4 = $_POST["duration"];
		$value5 = $_POST["height_difference"];
		$value6 = $_POST["available"];
		$sql = "INSERT INTO hiking (name, difficulty, distance, duration, height_difference, available) VALUE ('$value1', '$value2', '$value3', '$value4', '$value5', $value6)";
		$connect->exec($sql);
		echo "Succès !";

	}

	

	?>
</body>
</html>
